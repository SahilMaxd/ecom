var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');

router.post('/get', function(req, res, next) {
  mongoose.connect('mongodb://localhost/ecom', {useNewUrlParser: true});

  console.log('Grab profile associated with ' + req.body.username + ' from database');

  var products = req.app.get('products');

  products.find({}, function(err, data) {
    mongoose.connection.close();
    res.json({ products: data });
  });
});

module.exports = router;

/*
products : [{
  name: 'string',
  price: Number,
  Image: 'string'

}]
*/
