var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');

router.post('/get', function(req, res, next) {
  mongoose.connect('mongodb://localhost/ecom', {useNewUrlParser: true});

  console.log('Grab profile associated with ' + req.body.username + ' from database');

  var profiles = req.app.get('profiles');

  profiles.find({
    username: req.body.username
  }, function(err, data) {
    mongoose.connection.close();
    if (data.length > 0)
      res.json(data[0]);
    else
      res.json({
        username: '',
        name: '',
        email: '',
        address: '',
        card: '',
        cvv: '',
      });
  });
});

router.post('/post', function(req, res, next) {
  mongoose.connect('mongodb://localhost/ecom', { useNewUrlParser: true});

  var result = '';
  var profiles = req.app.get('profiles');

  var username = req.body.username;
  var profile = req.body.profile;
  profile.username = username;

  if (!profile.name || !profile.email || !profile.address || !profile.card || !profile.cvv) {
    result = 'fail';
    res.json({
      result: result
    });
  }
  else if (profile.name.length > 64 || profile.email.length > 32 || profile.address.length > 256 || profile.card.length > 16 || profile.cvv.length > 8) {
    result = 'fail';
    res.json({
      result: result
    });
  }
  else
    profiles.find({
      username: username
    }, function(err, data) {
      if (data.length > 0){
        newProfile = data[0];
        newProfile.name = profile.name;
        newProfile.email = profile.email;
        newProfile.address = profile.address;
        newProfile.card = profile.card;
        newProfile.cvv = profile.cvv;

        newProfile.save(function(err, data) {
          mongoose.connection.close();

          if (err)
            result = 'fail';
          else
            result = 'success';

          res.json({
            result: result
          });
        });
      }
      else
        profiles.create(profile, function(err, data) {
          mongoose.connection.close();

          if (err)
            result = 'fail';
          else
            result = 'success';

          res.json({
            result: result
          });
        });
    });
});

module.exports = router;
