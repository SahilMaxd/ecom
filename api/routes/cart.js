var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

router.post('/get', function(req, res, next) {
  mongoose.connect('mongodb://localhost/ecom', {useNewUrlParser: true});

  var cart = req.app.get('cart');

  cart.find({ username: req.body.username}, function(err, data) {
    mongoose.connection.close();
    if(data.length > 0)
      res.json({ cart: data[0].cart });
    else {
      res.json({ cart: []});
    }
  });
});

router.post('/post', function(req, res, next) {
  mongoose.connect('mongodb://localhost/ecom', {useNewUrlParser: true});

  var cart = req.app.get('cart');

  cart.find({ username: req.body.username}, function(err, data) {
    if(data.length > 0) {
      var newcart = data[0];

      var temp = [];
      temp = newcart.cart;
      temp.push(req.body.product);
      newcart.cart = temp;

      newcart.save(function(err, data) {
        mongoose.connection.close();

        if (err)
          result = 'fail';
        else
          result = 'success';

        res.json({
          result: result
        });
      });
    }
    else
      cart.create({ username: req.body.username, cart: [req.body.product] }, function(err, data) {
        mongoose.connection.close();
        if (err) {
          console.log(err);
          res.json({ result: 'failed' })
        }
        else
          res.json({ result: 'success' })
      });
  });
});

router.post('/delete', function(req, res, next) {
  mongoose.connect('mongodb://localhost/ecom', {useNewUrlParser: true});

  var cart = req.app.get('cart');

  cart.find({ username: req.body.username}, function(err, data) {
    if(data.length > 0) {
      var newcart = data[0];
      var temp = [];
      newcart.cart = temp;

      newcart.save(function(err, data) {
        mongoose.connection.close();

        if (err)
          result = 'fail';
        else
          result = 'success';

        res.json({
          result: result
        });
      });
    }
    else
      res.json({
        result: 'fail'
      });
  });
});

/*
{
  username: 'admin',
  cart: [
  'donuts',
  '1/2 dozen donuts'
]
}
*/

module.exports = router;
