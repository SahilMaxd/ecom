var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

router.post('/', function(req, res, next) {
  mongoose.connect('mongodb://localhost/ecom', {useNewUrlParser: true});

  console.log('verify the username and password provided is contained in database');
  var users = req.app.get('users')

  users.find({
    username: req.body.username,
    password: req.body.password
  }, function(err, data) {
    mongoose.connection.close();

    var result = '';
    var currentUser = '';
    if (data.length > 0){
      result = 'success';
      currentUser = req.body.username;
    }
    else {
      result = 'fail';
      currentUser = 'none';
    }

    res.json({
      result: result,
      currentUser: currentUser
    });
  });
});

module.exports = router;
