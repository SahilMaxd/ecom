var request = require('supertest');
var app = require('./app');
var http = require('http');
var expect = require('chai').expect;

describe('API server', function() {
  var server;

  before(function(done) {
    server = app.listen(4400, function(err) {
        if (err)
          return done(err);
        done();
      });
  });

  it('should return successful with proper user account', function(done) {
    request(app)
      .post('/api/auth/')
      .send({username: 'admin', password: 'password'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('success');
        done();
      });
  });

  it('should return failed with improper user account', function(done) {
    request(app)
      .post('/api/auth/')
      .send({username: 'someone', password: 'asdfasdf'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  it('should return successful with proper address', function(done) {
    request(app)
      .post('/api/checkout/order')
      .send({address: "13302 Ridgewood Knoll Ln, Houston, TX"})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('Success');
        done();
      });
  });


  it('should return failed with improper address', function(done) {
    request(app)
      .post('/api/checkout/order')
      .send({address: "13302 Ridgewood Knoll Ln, TX"})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('Fail');
        done();
      });
  });


  it('should return json object containing profile', function(done) {
    request(app)
      .post('/api/profile/get/')
      .send({username: "admin"})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.cvv;
        expect(result).to.equal('123');
        done();
      });
  });

  it('should not return json object containing profile', function(done) {
    request(app)
      .post('/api/profile/get/')
      .send({username: "someonerandom"})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.cvv;
        expect(result).to.equal('');
        done();
      });
  });

  it('should return successful with proper profile attributes', function(done) {
    request(app)
      .post('/api/profile/post/')
      .send({username: "usersome", profile: {name: "Cristian", email: "asd@gmail.com", address: "1234 somewhere", card: "123456", cvv: "123"}})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('success');
        done();
      });
  });

  it('should return fail with blank profile attributes', function(done) {
    request(app)
      .post('/api/profile/post/')
      .send({username: "", profile: {}})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  it('should return fail with oversized profile attributes', function(done) {
    request(app)
      .post('/api/profile/post/')
      .send({username: "usersome", profile: {name: "aaaaa", email: "bbbbbbbbbbbbbbbbbbbbbbb", address: "123 somewhere, Houston, TX", cardnumber: "123455", cvv: "12ertrtuen67uasdatq543"}})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  it('should return success with proper registration attributes', function(done) {
    request(app)
      .post('/api/register/post/')
      .send({username: "qwer", password: "password"})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('success');
        done();
      });
  });

  it('should return fail with improper registration attributes', function(done) {
    request(app)
      .post('/api/register/post/')
      .send({username: "", password: ""})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  it('should return fail with oversize registration attributes', function(done) {
    request(app)
      .post('/api/register/post/')
      .send({username: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", password: "123"})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  it('should return price with finding of product', function(done) {
    request(app)
      .post('/api/products/get/')
      .send({name: 'Dozen Donuts', price: 10.50, Image: 'https://images.squarespace-cdn.com/content/v1/5a340fb5b7411c1532a36c54/1513391648990-Q23626EFGIPWGJR80JUV/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/0018VG_Donutspf.jpg?format=2500w'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.products[2].price;
        expect(result).to.equal(10.50);
        done();
      });
  });

  it('should return empty with finding of product', function(done) {
    request(app)
      .post('/api/products/get/')
      .send({name: 'Product Not in There', price: 111.50, Image: 'https://images.something.com'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.products;
        expect(result).to.deep.equal([]);
        done();
      });
  });

  //add to cart
  // /api/cart/get

  it('should get cart', function(done) {
    request(app)
      .post('/api/cart/get/')
      .send({ username: 'admin'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var cart = res.body.cart[0];
        expect(cart).to.equal("Dozen Donuts");
        done();
      });
  });

  it('should not get cart', function(done) {
    request(app)
      .post('/api/cart/get/')
      .send({ username: 'admefsfsdfdsfdsdin'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var cart = res.body.cart;
        expect(cart).to.deep.equal([]);
        done();
      });
  });


  it('should return with successful with proper attributes', function(done) {
    request(app)
      .post('/api/cart/post/')
      .send({ username: 'admin'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('success');
        done();
      });
  });

  it('should return with successful with proper attributes', function(done) {
    request(app)
      .post('/api/cart/delete/')
      .send({ username: 'admin'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('success');
        done();
      });
  });

  it('should return with fail with improper attributes', function(done) {
    request(app)
      .post('/api/cart/delete/')
      .send({ username: 'oiusigudfhogiudsfhhu'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  after(function() {
    server.close();
  })
});
