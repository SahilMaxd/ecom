import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string = 'web';
  isLoggedIn: boolean;
  isGuest: boolean;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.authService.isLoggedIn.subscribe(data => {
      if (data == 'user') {
        this.isLoggedIn = true;
        this.isGuest = false;
      }
      else if (data == 'guest') {
        this.isLoggedIn = false;
        this.isGuest = true;
      }
      else if (data == 'none') {
        this.isLoggedIn = false;
        this.isGuest = false;
      }
    })
  }

  logout() {
    this.authService.logout();
  }
}
