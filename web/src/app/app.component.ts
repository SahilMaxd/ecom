import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  API_URL: string = 'http://localhost:4400/api';
  title: string = 'web';
  isLoggedIn: boolean;
  isGuest: boolean;
  username: string = '';

  cart: any;
  amount: Number = 0;

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.authService.isLoggedIn.subscribe(data => {
      if (data == 'user') {
        this.isLoggedIn = true;
        this.isGuest = false;
        this.username = localStorage.getItem('currentUser');
        this.getCart();
      }
      else if (data == 'guest') {
        this.isLoggedIn = false;
        this.isGuest = true;
        this.username = '';
        this.getCart();
      }
      else if (data == 'none') {
        this.isLoggedIn = false;
        this.isGuest = false;
        this.username = '';
      }
    })
  }

  getCart() {
    if (this.isLoggedIn == true)
      this.http.post(this.API_URL + '/cart/get', { username: localStorage.getItem('currentUser') })
        .subscribe((data: any) => {
          this.cart = data.cart;
          this.amount = data.cart.length;
        });
    else {
      if(sessionStorage.getItem('cart'))
        this.amount = JSON.parse(sessionStorage.getItem('cart')).length;
    }
  }

  logout() {
    this.authService.logout();
  }
}
