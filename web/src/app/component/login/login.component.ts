import { Component, OnInit} from '@angular/core';

import { AuthService } from '../../service/auth.service';
import { User } from '../../interface/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  login() {
    if (this.username && this.password) {
      let user: User = {
        username: this.username,
        password: this.password
      };
      this.authService.login(user);
    }
    else
      console.log('make sure username and password fields are entered');
  }

  guest() {
    this.authService.guest();
  }
}
