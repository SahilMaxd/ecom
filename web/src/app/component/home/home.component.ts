import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  API_URL: string = 'http://localhost:4400/api';
  products = [];
  isLoggedIn: boolean = false;
  isGuest: boolean = false;

  constructor(private http: HttpClient, private authService: AuthService) { }

  ngOnInit(): void {
    this.getProduct();
    this.authService.isLoggedIn.subscribe(data => {
      if (data == 'user') {
        this.isLoggedIn = true;
        this.isGuest = false;
      }
      else if (data == 'guest') {
        this.isLoggedIn = false;
        this.isGuest = true;
      }
      else if (data == 'none') {
        this.isLoggedIn = false;
        this.isGuest = false;
      }
    })
  }

  getProduct() {
    this.http.post(this.API_URL + '/products/get', { username: localStorage.getItem('currentUser') })
      .subscribe((data: any) => {
        this.products = data.products;
      });
  }

  postCart(product) {
    if (this.isLoggedIn == true)
      this.http.post(this.API_URL + '/cart/post', { username: localStorage.getItem('currentUser'), product: product })
        .subscribe((data: any) => {
          console.log(data.result);
        });
    else{
      if (sessionStorage.getItem('cart')) {
        var temp = JSON.parse(sessionStorage.getItem('cart'));
        temp.push(product);
        sessionStorage.setItem('cart', JSON.stringify(temp));
      }
      else
        sessionStorage.setItem('cart', JSON.stringify([product]));
    }
  }

}
