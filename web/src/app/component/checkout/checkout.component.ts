import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../service/auth.service';
import "smartystreets-javascript-sdk";
import * as SmartyStreetsSDK from "smartystreets-javascript-sdk";
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  API_URL: string = 'http://localhost:4400/api';
  show_Checkout: boolean = true;
  name = ''
  email = ''
  address = ''
  card = '6669420'
  products = [];
  sum = 0
  cvv = "000";
  enteredcvv = "";
  isLoggedIn: boolean;
  isGuest: boolean;
  bad_Address: boolean = false;
  cvv_button_text = "Verify CVV"
  cvv_auth:boolean = false;
  orderConf = "Place Your Order"


  constructor(private http: HttpClient, private authService: AuthService) { }

  ngOnInit(): void {
    this.show_Checkout = true;
    this.cvv_button_text = "Verify CVV"
    this.cvv_auth = false;
    this.orderConf = "Place Your Order"
    this.bad_Address = false;

    this.getProfile();
    this.getData();

    this.authService.isLoggedIn.subscribe(data => {
      if (data == 'user') {
        this.isLoggedIn = true;
        this.isGuest = false;
      }
      else if (data == 'guest') {
        this.isLoggedIn = false;
        this.isGuest = true;
      }
    })

  }

  getProfile() {
    this.http.post(this.API_URL + '/profile/get', { username: localStorage.getItem('currentUser') })
      .subscribe((data: any) => {
        this.name= data.name;
        this.email = data.email;
        this.address = data.address;
        this.card = data.card;
        this.cvv = data.cvv;
      });
  }

  getData() {
    this.http.post(this.API_URL + '/products/get', { username: localStorage.getItem('currentUser') })
      .subscribe((products_data: any) => {

        if(this.isLoggedIn == true)
          this.http.post(this.API_URL + '/cart/get', { username: localStorage.getItem('currentUser') })
            .subscribe((cart_data: any) => {
              var cart = cart_data.cart;
              var products = products_data.products;

              for (var item of cart) {
                for (var product of products) {
                  if (product.name == item) {
                    this.products.push(product);
                    this.sum = this.sum + product.price;
                  }
                }
              }
            });
        else{
          var cart = JSON.parse(sessionStorage.getItem('cart'));
          var products = products_data.products;

          for (var item of cart) {
            for (var product of products) {
              if (product.name == item) {
                this.products.push(product);
                this.sum = this.sum + product.price;
              }
            }
          }
        }
      });
  }

  cvv_buttonClick(){
    if (this.enteredcvv == this.cvv){
      this.cvv_button_text = "Verified";
      this.cvv_auth = true;
    }
    else{
      this.cvv_button_text = "You entered wrong cvv.";
    }
  }
  timeLeft = 1;
  interval;

  placeOrder(){
    this.http.post(this.API_URL + '/checkout/order', { username: localStorage.getItem('currentUser') , address: this.address, name: this.name, card: this.card, email: this.email, cvv: this.enteredcvv})
      .subscribe((data: any) => {
        if (data.result == "Success"){
          this.bad_Address = false;
          this.orderConf = "Placing Order...";
          this.interval = setInterval(() => {
            if(this.timeLeft > 0){
              this.timeLeft--;
            }
            else{
              this.show_Checkout = false;

              if(this.isLoggedIn == true)
                this.http.post(this.API_URL + '/cart/delete', { username: localStorage.getItem('currentUser') })
                  .subscribe((data: any) => {
                    console.log(data.result);
                  });
              else
                sessionStorage.removeItem('cart');
            }
          },1000)
        }
        else{
          console.log("Failed to place order");
          this.bad_Address = true;
        }
      });

  }

  addressValidator(){


  }

}
