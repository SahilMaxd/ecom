import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Profile } from '../../interface/profile';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  API_URL: string = 'http://localhost:4400/api';
  name: string = '';
  email: string = '';
  address: string = '';
  card: string = '';
  cvv: string = '';

  disabled: boolean = true;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.disabled = true;
    this.getProfile();
  }

  getProfile() {
    this.http.post(this.API_URL + '/profile/get', { username: localStorage.getItem('currentUser') })
      .subscribe((data: Profile) => {
        this.name = data.name;
        this.email = data.email;
        this.address = data.address;
        this.card = data.card;
        this.cvv = data.cvv;
      });
  }

  postProfile() {
    if (this.name && this.email && this.address && this.card && this.cvv) {
      let profile: Profile = {
        name: this.name,
        email: this.email,
        address: this.address,
        card: this.card,
        cvv: this.cvv
      }
      this.http.post(this.API_URL + '/profile/post', { username: localStorage.getItem('currentUser'), profile: profile })
        .subscribe((data: any) => {
          console.log(data.result);
        })
    }
    else
      console.log('make sure required fields are entered');
  }
}
