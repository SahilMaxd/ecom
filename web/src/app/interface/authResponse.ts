export interface AuthResponse {
  result: string;
  currentUser: string;
}
