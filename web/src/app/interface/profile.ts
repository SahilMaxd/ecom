export interface Profile {
  name: string;
  email: string;
  address: string;
  card: string;
  cvv: string;
}
