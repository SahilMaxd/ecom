import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { BehaviorSubject} from 'rxjs';
import { map, take } from 'rxjs/operators';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthgaurdService implements CanActivate {
  isLoggedIn: BehaviorSubject<string>;

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.isLoggedIn = this.authService.isLoggedIn;

    if (state.url == '/login' || state.url == '/register'){
      if (this.isLoggedIn.value == 'none')
        return true;
      this.router.navigate(['/']);
      return false;
    }
    if (state.url == '/profile'){
      if (this.isLoggedIn.value == 'user')
        return true;
      if (this.isLoggedIn.value == 'guest')
        this.router.navigate(['/']);
        return false;
    }
    if (this.isLoggedIn.value == 'user' || this.isLoggedIn.value == 'guest')
      return true;
    this.router.navigate(['/login']);
    return false;
  }
}
