import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { User } from '../interface/user';
import { AuthResponse} from '../interface/authResponse';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loggedIn: BehaviorSubject<string>;
  API_URL: string = 'http://localhost:4400/api';

  constructor(private http: HttpClient, private router: Router) {
    this.initialize();
  }

  initialize() {
    if (localStorage.getItem('currentUser'))
      this.loggedIn = new BehaviorSubject<string>('user');
    else if (sessionStorage.getItem('currentUser'))
      this.loggedIn = new BehaviorSubject<string>('guest')
    else
      this.loggedIn = new BehaviorSubject<string>('none');
  }

  get isLoggedIn() {
    return this.loggedIn;
  }

  login(user: User) {
    if (user.username && user.password)
      this.http.post(this.API_URL + '/auth', user)
        .subscribe((data: AuthResponse) => {
          if (data.result == 'success') {
            localStorage.setItem('currentUser', data.currentUser);
            sessionStorage.removeItem('currentUser');
            this.loggedIn.next('user');
            this.router.navigate(['/']);
          }
          else
            console.log(data.result);
        });
  }

  logout() {
    localStorage.removeItem('currentUser');
    sessionStorage.removeItem('currentUser');
    this.loggedIn.next('none');
    this.router.navigate(['/login'])
  }

  guest() {
    sessionStorage.setItem('currentUser', 'guest');
    this.loggedIn.next('guest');
    this.router.navigate(['/']);
  }
}
