import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { RegistrationComponent } from './component/registration/registration.component';
import { CartComponent } from './component/cart/cart.component';
import { CheckoutComponent } from './component/checkout/checkout.component';
import { ProfileComponent } from './component/profile/profile.component';
import { AuthgaurdService } from './service/authgaurd.service';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthgaurdService]},
  { path: 'login', component: LoginComponent, canActivate: [AuthgaurdService]},
  { path: 'register', component: RegistrationComponent, canActivate: [AuthgaurdService]},
  { path: 'cart', component: CartComponent, canActivate: [AuthgaurdService]},
  { path: 'checkout', component: CheckoutComponent, canActivate: [AuthgaurdService]},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthgaurdService]},
  { path: '**', redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
